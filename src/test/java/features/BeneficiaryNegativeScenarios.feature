Feature: Validating Negative scenarios for Beneficiary

  Scenario Outline: Verify error messages while create beneficiary
    Given user gets access token using "KJbr_Xs5TLmhY03JWSi3NQ" and "2308e2dd300f6b959b7f4e0a52ba9181186fc92f075f7e64ee9fa0b6b1ada094c39c9a9f39f06693d06b17067f78d4e7"
    When user call "login" with "POST" http request
    Then the API call should get success with status code 201
    Given Beneficiary details are provided using array
      | personal_email         | test@abhi.com            |
      | city                   | Seattle                  |
      | country_code           | US                       |
      | postcode               | Seattle                  |
      | state                  | Washington               |
      | street_address         | 412 5th Avenue           |
      | account_currency       | <account_currency>       |
      | account_name           | <account_name>           |
      | account_number         | <account_number>         |
      | account_routing_type1  | <account_routing_type1>  |
      | account_routing_value1 | <account_routing_value1> |
      | bank_country_code      | <bank_country_code>      |
      | bank_name              | <bank_name>              |
      | swift_code             | <swift_code>             |
      | company_name           | Abhi Tech                |
      | entity_type            | COMPANY                  |
      | nickname               | ABT                      |
      | payer_entity_type      | PERSONAL                 |
      | payment_methods        | <payment_methods>        |
      | Scenario               | <Scenario>               |

    #When user call "validateBeneficiary" with "POST" http request
    #Then the API call should get success with status code 200
    When user call "createBeneficiary" with "POST" http request
    Then the API call should get success with status code <Status>
    Then validate API response error "<Error>"

    Examples:


      | ######Scenario######                                       | Status | Error                                                                    | account_currency | account_name | account_number                              | account_routing_type1 | account_routing_value1 | bank_country_code | bank_name            | swift_code | payment_methods |
      | No Payment method                                          | 400    | No enum constant com.airwallex.domain.transaction.payment.PaymentMethod. | USD              | John Walker  | 50001121                                    | aba                   | 021000021              | US                | JP Morgan Chase Bank | CHASUS33   |                 |
      | No Bank Country Code                                       | 400    | is not a valid type                                                      | USD              | John Walker  | 50001121                                    | aba                   | 021000021              |                   | JP Morgan Chase Bank | CHASUS33   | SWIFT           |
      | Wrong Bank Country code                                    | 400    | ABC is not a valid type                                                  | USD              | ABHISHEK     | 50001121                                    | aba                   | 021000021              | ABC               | ABC    Bank          | CHASUS33   | SWIFT           |
      | No Account Name                                            | 400    | This field is required                                                   | USD              |              | 50001121                                    | aba                   | 021000021              | US                | JP Morgan Chase Bank | CHASUS33   | SWIFT           |
      | Account Name =1                                            | 400    | Should contain 2 to 10 characters                                        | USD              | 1            | 50001121                                    | aba                   | 021000021              | US                | JP Morgan Chase Bank | CHASUS33   | SWIFT           |
      | US Account Nr >17                                          | 400    | Should contain 1 to 17 characters                                        | USD              | ABHISHEK     | 123456789999999999999999999                 | aba                   | 021000021              | US                | JP Morgan Chase Bank | CHASUS33   | SWIFT           |
      | US Account Nr =0                                           | 400    | This field is required                                                   | USD              | ABHISHEK     |                                             | aba                   | 021000021              | US                | JP Morgan Chase Bank | CHASUS33   | SWIFT           |
      | CN Account Nr >20                                          | 400    | Should contain 8 to 20 characters                                        | USD              | ABHISHEK     | 123456789ABCOKLPOKJNB1111111111111111111111 | aba                   | 021000021              | CN                | JP Morgan Chase Bank | ICBKCNBJ   | SWIFT           |
      | CN Account Nr <8                                           | 400    | Should contain 8 to 20 characters                                        | USD              | ABHISHEK     | 1212321                                     | aba                   | 021000021              | CN                | JP Morgan Chase Bank | ICBKCNBJ   | SWIFT           |
      | CN Account Nr =0                                           | 400    | This field is required                                                   | USD              | ABHISHEK     |                                             | aba                   | 021000021              | CN                | JP Morgan Chase Bank | ICBKCNBJ   | SWIFT           |
      | AU Account Nr >22                                          | 400    | Should contain 6 to 9 characters                                         | USD              | ABHISHEK     | 12345678911111111111111                     | aba                   | 021000021              | AU                | JP Morgan Chase Bank | ANZBAU3M   | SWIFT           |
      | AU Account Nr <6                                           | 400    | Should contain 6 to 9 characters                                         | USD              | ABHISHEK     | ABC12                                       | aba                   | 021000021              | AU                | JP Morgan Chase Bank | ANZBAU3M   | SWIFT           |
      | AU Account Nr =0                                           | 400    | This field is required                                                   | USD              | ABHISHEK     |                                             | aba                   | 021000021              | AU                | JP Morgan Chase Bank | ANZBAU3M   | SWIFT           |
      | SWIFT - No Swift code                                      | 400    | This field is required                                                   | USD              | ABHISHEK     | ABC1234                                     | aba                   | 021000021              | AU                | JP Morgan Chase Bank |            | SWIFT           |
      | SWIFT - Wrong Swift code                                   | 400    | Should be a valid and supported SWIFT code / BIC                         | USD              | ABHISHEK     | ABC1234                                     | aba                   | 021000021              | AU                | JP Morgan Chase Bank | ANZBCN3M   | SWIFT           |
      | AU Account, account_routing_type1=Empty                    | 400    | is not a valid type                                                      | USD              | ABHISHEK     | ABC1234                                     |                       |                        | AU                | JP Morgan Chase Bank | ANZBAU3M   | SWIFT           |
      | US Account, account_routing_type1=Empty                    | 400    | is not a valid type                                                      | USD              | ABHISHEK     | ABC1234                                     |                       |                        | US                | JP Morgan Chase Bank | CHASUS33   | SWIFT           |
      | AU Account,account_routing_type1=Incorrect                 | 400    | abc is not a valid type                                                  | USD              | ABHISHEK     | ABC1234                                     | abc                   |                        | AU                | JP Morgan Chase Bank | ANZBAU3M   | SWIFT           |
      | US Account,account_routing_type1=Incorrect                 | 400    | abc is not a valid type                                                  | USD              | ABHISHEK     | ABC1234                                     | abc                   |                        | US                | JP Morgan Chase Bank | CHASUS33   | SWIFT           |
      | CN Account,account_routing_type1=Empty                     | 400    | is not a valid type                                                      | USD              | ABHISHEK     | ABC1234                                     |                       |                        | AU                | JP Morgan Chase Bank | ANZBAU3M   | SWIFT           |
      | CN Account,account_routing_type1=Incorrect                 | 400    | abc is not a valid type                                                  | USD              | ABHISHEK     | ABC1234                                     | abc                   |                        | US                | JP Morgan Chase Bank | CHASUS33   | SWIFT           |
      | AU Account,account_routing_type1=Correct, value1=Empty     | 400    |                                                                          | USD              | ABHISHEK     | ABC1234                                     | bsb                   |                        | AU                | JP Morgan Chase Bank | ANZBAU3M   | SWIFT           |
      | AU Account,account_routing_type1=Correct, value1=Incorrect | 400    |                                                                          | USD              | ABHISHEK     | ABC1234                                     | bsb                   | 123                    | AU                | JP Morgan Chase Bank | ANZBAU3M   | SWIFT           |
      | CN Account,account_routing_type1=Correct, type1=Empty      | 400    |                                                                          | USD              | ABHISHEK     | ABC1234                                     | cnaps                 |                        | AU                | JP Morgan Chase Bank | ANZBAU3M   | SWIFT           |
      | CN Account,account_routing_type1=Correct, type1=Incorrect  | 400    |                                                                          | USD              | ABHISHEK     | ABC1234                                     | cnaps                 | 123                    | US                | JP Morgan Chase Bank | CHASUS33   | SWIFT           |
      | US Account,account_routing_type1=Correct, type1=Empty      | 400    |                                                                          | USD              | ABHISHEK     | ABC1234                                     | aba                   |                        | US                | JP Morgan Chase Bank | CHASUS33   | SWIFT           |
      | US Account,account_routing_type1=Correct ,type1=Incorrect  | 400    | Should be 9 characters long                                              | USD              | ABHISHEK     | ABC1234                                     | aba                   | 123                    | US                | JP Morgan Chase Bank | CHASUS33   | SWIFT           |