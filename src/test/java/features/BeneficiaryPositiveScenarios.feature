Feature: Validating Positive scenarios for Beneficiary

  Scenario Outline: Verify if beneficiary can be created
    Given user gets access token using "KJbr_Xs5TLmhY03JWSi3NQ" and "2308e2dd300f6b959b7f4e0a52ba9181186fc92f075f7e64ee9fa0b6b1ada094c39c9a9f39f06693d06b17067f78d4e7"
    When user call "login" with "POST" http request
    Then the API call should get success with status code 201
    Given Beneficiary details are provided using array
      | personal_email         | test@abhi.com            |
      | city                   | Seattle                  |
      | country_code           | US                       |
      | postcode               | Seattle                  |
      | state                  | Washington               |
      | street_address         | 412 5th Avenue           |
      | account_currency       | <account_currency>       |
      | account_name           | <account_name>           |
      | account_number         | <account_number>         |
      | account_routing_type1  | <account_routing_type1>  |
      | account_routing_value1 | <account_routing_value1> |
      | bank_country_code      | <bank_country_code>      |
      | bank_name              | <bank_name>              |
      | swift_code             | <swift_code>             |
      | company_name           | Abhi Tech                |
      | entity_type            | COMPANY                  |
      | nickname               | ABT                      |
      | payer_entity_type      | PERSONAL                 |
      | payment_methods        | <payment_methods>        |

    When user call "validateBeneficiary" with "POST" http request
    Then the API call should get success with status code 200
    When user call "createBeneficiary" with "POST" http request
    Then the API call should get success with status code 201

    Examples:
      | ######Scenario######                       | account_name | account_number       | account_routing_type1 | account_routing_value1 | bank_country_code | bank_name                   | swift_code  | payment_methods | account_currency |
      | bcc=US, sc=8 digits, acc.no=1digits, aba   | AB           | 1                    | aba                   |                        | US                | eyJqdGkiOiIyYzg3ZTZmMS05NjJ | CHASUS33    | SWIFT           | USD              |
      | bcc=CN, sc=8digits, acc.no=8digits,cnaps   | AB           | 12345678             | cnaps                 |                        | CN                | JP Morgan Chase Bank        | ICBKCNBJ    | SWIFT           | EUR              |
      | bcc=AU, sc=8digits, acc.no=6digits, bsb    | AB           | 123456               | bsb                   |                        | AU                | JP Morgan Chase Bank        | ANZBAU3M    | SWIFT           | AUD              |
      | bcc=US, sc=11digits, acc.no=17digits, aba  | ABHISHEK T   | 11111111111111111    | aba                   |                        | US                | eyJqdGkiOiIyYzg3ZTZmMS05NjJ | PMFAUS66HKG | SWIFT           | USD              |
      | bcc=CN, sc=11digits, acc.no=20digits,cnaps | ABHISHEK T   | 123456789098765EDCBA | cnaps                 |                        | CN                | JP Morgan Chase Bank        | ABOCCNBJ021 | SWIFT           | EUR              |
      | bcc=AU, sc=11digits, acc.no=9digits, bsb   | ABHISHEK T   | 123456789            | bsb                   |                        | AU                | JP Morgan Chase Bank        | AMPBAU2SRET | SWIFT           | AUD              |
      | bcc=US, pm=local, sc=BLANK                 | ABHISHEK     | 50001121             | aba                   | 031201360              | US                | JP Morgan Chase Bank        |             | LOCAL           | USD              |




    ##bcc-bankCountryCode
    ##sc-swiftcode
    ##acc.no- Account Number
    ##pm=PaymentMethod