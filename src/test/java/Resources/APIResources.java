package Resources;

//enum is special class in java which has collection of constants or methods

/**
 * This class helps to declare API resources in a constant
 * which can be directly pass to the Scenarios in Feature file
 **/

public enum APIResources {

    login("authentication/login"),
    createBeneficiary("beneficiaries/create"),
    validateBeneficiary("beneficiaries/validate");

    private String resource;

    APIResources(String resource) {
        this.resource = resource;
    }

    public String getResource() {
        return resource;
    }

}
