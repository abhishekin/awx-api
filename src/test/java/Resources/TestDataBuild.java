package Resources;


import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.Map;


public class TestDataBuild {

    JSONObject FinalJson = new JSONObject();
    ArrayList<String> list = new ArrayList<String>();

    /**
     * This is method to send JSON body for creating beneficiary
     */
    @SuppressWarnings("unchecked")
    public JSONObject createBeneficiary(Map<String, String> beneficiary) {
        FinalJson.clear();

        JSONObject Beneficiary = new JSONObject();

        JSONObject additionalInfo = new JSONObject();
        additionalInfo.put("personal_email", beneficiary.get("personal_email"));

        Beneficiary.put("additional_info", additionalInfo);

        JSONObject address = new JSONObject();
        address.put("city", beneficiary.get("city"));
        address.put("country_code", beneficiary.get("country_code"));
        address.put("postcode", beneficiary.get("postcode"));
        address.put("state", beneficiary.get("state"));
        address.put("street_address", beneficiary.get("street_address"));

        Beneficiary.put("address", address);

        JSONObject bankdetails = new JSONObject();
        bankdetails.put("account_currency", beneficiary.get("account_currency"));
        bankdetails.put("account_name", beneficiary.get("account_name"));
        bankdetails.put("account_number", beneficiary.get("account_number"));
        bankdetails.put("account_routing_type1", beneficiary.get("account_routing_type1"));
        bankdetails.put("account_routing_value1", beneficiary.get("account_routing_value1"));
        bankdetails.put("bank_country_code", beneficiary.get("bank_country_code"));
        bankdetails.put("bank_name", beneficiary.get("bank_name"));
        bankdetails.put("swift_code", beneficiary.get("swift_code"));

        Beneficiary.put("bank_details", bankdetails);
        Beneficiary.put("company_name", beneficiary.get("company_name"));
        Beneficiary.put("entity_type", beneficiary.get("entity_type"));


        FinalJson.put("beneficiary", Beneficiary);
        FinalJson.put("nickname", beneficiary.get("nickname"));
        FinalJson.put("payer_entity_type", beneficiary.get("payer_entity_type"));

        list.add(beneficiary.get("payment_methods"));
        FinalJson.put("payment_methods", list);
        return FinalJson;

    }
}
