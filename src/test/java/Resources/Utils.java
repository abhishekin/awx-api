package Resources;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Properties;


/**
 * This is the base class where reusable methods and variables are declared
 */

public class Utils {

    public static RequestSpecification req;
    public static RequestSpecification res;
    public static Response response;
    public TestDataBuild data = new TestDataBuild();


    /**
     * This is the reusable method used in every specifying every request
     * along with logging the request and response in logging.txt file
     */
    public RequestSpecification requestSpecification() throws IOException {
        if (req == null) {
            PrintStream log = new PrintStream(new FileOutputStream("logging.txt"));
            req = new RequestSpecBuilder().setBaseUri(getGlobalValue("baseURL"))
                    .addFilter(RequestLoggingFilter.logRequestTo(log))
                    .addFilter(ResponseLoggingFilter.logResponseTo(log))
                    .setAccept(ContentType.JSON).build();
        }
        return req;
    }


    /**
     * This reusable method is used to get the value of parameter declared in global.properties file
     */
    public String getGlobalValue(String key) throws IOException {

        Properties prop = new Properties();
        FileInputStream fs = new FileInputStream("global.properties");
        prop.load(fs);
        return prop.getProperty(key);
    }

    /**
     * This reusable method is used to get the attribute value of response body
     */
    public String getJsonPath(Response response, String key) {
        String resp = response.asString();
        JsonPath js = new JsonPath(resp);
        return js.get(key).toString();
    }
}
