package stepDefinations;

import Resources.APIResources;
import Resources.Utils;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.http.ContentType;

import java.io.IOException;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;


public class StepDefinationsBeneficiary extends Utils {


    String Token;

    @Given("user gets access token using {string} and {string}")
    public void userGetsAccessTokenUsingAnd(String xclientid, String xapikey) throws IOException {
        res = given()
                .header("x-client-id", xclientid)
                .header("x-api-key", xapikey)
                .spec(requestSpecification())
                .contentType(ContentType.JSON);
    }

    @Given("Beneficiary details are provided using array")
    public void Beneficiary_details_are_provided_using_array(Map<String, String> user) throws IOException {
        res = given()
                .headers("Authorization", "Bearer " + Token)
                .spec(requestSpecification())
                .contentType(ContentType.JSON).
                        body(data.createBeneficiary(user));
    }


    @When("user call {string} with {string} http request")
    public void user_call_with_http_request(String resource, String method) {
        response = null;
        APIResources resourceAPI = APIResources.valueOf(resource);
        if (method.equalsIgnoreCase("POST"))
            response = res.when().post(resourceAPI.getResource());
        else if (method.equalsIgnoreCase("GET"))
            response = res.when().get(resourceAPI.getResource());
        else if (method.equalsIgnoreCase("DELETE"))
            response = res.when().delete(resourceAPI.getResource());
        else if (method.equalsIgnoreCase("PUT"))
            response = res.when().put(resourceAPI.getResource());
        if (resource.equals("login")) {
            Token = getJsonPath(response, "token");
        }
    }


    @Then("the API call should get success with status code {int}")
    public void the_api_call_should_get_success_with_status_code(int statuscode) {
        assertEquals(statuscode, response.getStatusCode());
    }


    @Then("validate API response error {string}")
    public void validateAPIResponseError(String error) {
        String errorMsg=getJsonPath(response, "message");
        assertEquals(error, errorMsg.trim());
    }

}
