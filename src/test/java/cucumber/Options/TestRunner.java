package cucumber.Options;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;


@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/java/features/", plugin = {
        "pretty",
        "json:target/cucumber.json", "html:target/default-html-reports", "rerun:target/rerun.txt"}, 
		glue= {"stepDefinations"})
public class TestRunner {

}
